Summary: Filesystem and RPM macros for KDE Frameworks 5
Name:    kf5-rpm-macros
Version: 5.110.0
Release: 2%{?dist}
License: BSD
URL:     http://www.kde.org
Source0: macros.kf5
Requires: cmake >= 3
Requires: qt5-rpm-macros >= 5.11
BuildArch: noarch



%description
RPM macros for building KDE Frameworks 5 packages.

%package -n kf5-filesystem
Summary: Filesystem for KDE Frameworks 5
%{?_qt5_version:Requires: qt5-qtbase >= %{_qt5_version}}

%description -n kf5-filesystem
Filesystem for KDE Frameworks 5.



%install
mkdir -p %{buildroot}%{_prefix}/{lib,%{_lib}}/qt5/plugins/kf5/
mkdir -p %{buildroot}%{_includedir}/KF5
mkdir -p %{buildroot}%{_datadir}/{kf5,kservicetypes5}
mkdir -p %{buildroot}%{_datadir}/kservices5/ServiceMenus
mkdir -p %{buildroot}%{_datadir}/qlogging-categories5/
mkdir -p %{buildroot}%{_docdir}/qt5
mkdir -p %{buildroot}%{_libexecdir}/kf5
install -Dpm644 %{_sourcedir}/macros.kf5 %{buildroot}%{_rpmconfigdir}/macros.d/macros.kf5
sed -i \
  -e "s|@@KF5_VERSION@@|%{version}|g" \
  %{buildroot}%{_rpmconfigdir}/macros.d/macros.kf5



%files
%{_rpmconfigdir}/macros.d/macros.kf5

%files -n kf5-filesystem
%{_prefix}/lib/qt5/plugins/kf5/
%{_prefix}/%{_lib}/qt5/plugins/kf5/
%{_includedir}/KF5/
%{_libexecdir}/kf5/
%{_datadir}/kf5/
%{_datadir}/kservices5/
%{_datadir}/kservicetypes5/
%{_datadir}/qlogging-categories5/
%{_docdir}/qt5/



%changelog
* Mon Oct 30 2023 Zhao Zhen <jeremiazhao@tencent.com> - 5.110.0-2
- rename source package name

* Tue Oct 17 2023 Chair <chairou@tencent.com> - 5.110.0-1
- Rebuild

